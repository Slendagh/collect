
//searching via API for searched word and getting all the data then displaying it in page
var resultats='';
var query='';
var form = document.getElementById('form')
form.addEventListener('submit', function(e){
    e.preventDefault();
    query = document.getElementById("query").value;
    //get data via API
    $.getJSON('https://api.unsplash.com/search/photos/?query='+query+'&client_id=5ee17b6f125ea3bbd6f432900fa4ee35f895cac87d9f752c2b307738c524fea4', function(data){
        console.log(data);
        resultats=data;
        
        document.getElementById("acceuil").style.display="none";
        document.getElementById("results").style.display="block";
        var rep = "<div class='row'>";
        
        for(var i=0; i<data.results.length; i++){
            rep+="<div class='images'><img src='"+data.results[i]['urls']["small"]+"' class='card-img-top' id='img_opacity' onclick='showBigger("+i+");'><div class='toBeShown' ><button data-hover="+data.results[i]['id']+" onclick='addToLibrary(this);' class='btn bnt-danger'>+library</button></div></div>";
        }
        rep+="</div>";        
        //check if the section is not empty
        if(document.getElementById('img_here').childNodes != null){
            document.getElementById('img_here').innerHTML="";
        }
        document.getElementById("img_here").innerHTML+=rep;
    });
})

//add to library the selected images
var tabLibrary = [];
function addToLibrary(elem){
    var id = elem.dataset.hover;//getting the img id
    console.log(id);
    //console.log(resultats)
    for(var i=0; i<resultats.results.length; i++){
        if(resultats.results[i]['id']==id){
            tabLibrary.push(resultats.results[i]);
            console.log('done')
            window.localStorage.setItem(id, JSON.stringify(tabLibrary));
            var d = document.getElementById("numberCart").innerHTML=" ("+tabLibrary.length+")";
            console.log(tabLibrary.length);
            console.log(d);
        }
    }
    
}

function to_library(){
    document.getElementById("results").style.display='none';
    document.getElementById('ChosenImages').style.display='block';
        if(document.getElementById('ChosenImages').childNodes != null){
            document.getElementById('ChosenImages').innerHTML="";
        }
        var rep="<div id='twoButtons'><button id='previousPage' onclick='route(\"ChosenImages\", \"results\")'; class='btn btn-primary'>previous page</button></div>";
        rep+="<div class='row' style='width:90%; margin:auto;'>";
        for(var i=0; i<tabLibrary.length; i++){
            rep+="<div class='card images' style='width: 18rem;'>";
                rep+="<img src="+tabLibrary[i]['urls']["small"]+" class='card-img-top' alt='...' onclick='showBigger("+i+");'><div class='toBeShown' ><button data-hover="+tabLibrary[i]['id']+" onclick='removeFromLibrary(this);' class='btn bnt-danger'>remove</button></div>";
                rep+="<div class='card-body'>";
                    rep+="<h5 class='card-title'>"+query+"</h5>";
                rep+="</div></div>";
        }
        rep+="</div>"
        if(tabLibrary.length == 0){
            rep += "<p style='margin=auto; width: 50%;'>Your library is empty =( </p>"; 
        }
        document.getElementById("ChosenImages").innerHTML+=rep;

}

//show modal of selected image

function showBigger(i){
    var modal = document.getElementById("myModal");
    console.log(modal);
    var modalImg = document.getElementById("img01");
    console.log(modalImg);
    var captionText = document.getElementById("caption");
    console.log(captionText);
    modal.style.display = "block";
    console.log("3");
    modalImg.src = resultats.results[i]['urls']['regular'];
    console.log(modalImg.src);
    captionText.innerHTML = query;    
    console.log("5");
}

function closeModal(){
    var modal = document.getElementById("myModal");
    modal.style.display = "none";
}


function removeFromLibrary(elem){
    var imgToRemove = [];
    var id = elem.dataset.hover;
    console.log(id);
    for(var i=0; i<resultats.results.length; i++){
        if(resultats.results[i]['id']==id){
            var index = tabLibrary.indexOf(resultats.results[i]);
            if(index >-1){
                tabLibrary.splice(index, 1);
            }
            console.log(index);
            imgToRemove.push(resultats.results[i]);
            window.localStorage.removeItem(id, JSON.stringify(imgToRemove));
            alert("image removed successfully");
            to_library();
        }
    }
}

function route(from, to){
    document.getElementById(from).style.display = 'none';
    document.getElementById(to).style.display = 'block';
}